prfLikelihoodCI_newton_step <- function(max_iter, eps, damping, l_star, theta, ind, fn, gr, list_args, gndeps, hndeps, cl, trace, nreport) {
    for (i in 1:max_iter) {
        grad2 <- -calc_gradient(fn, gr, theta, gndeps, list_args, cl)
        if (any(!is.finite(grad2))) {
            stop(sprintf("Gradient is not finite; iteration %d", i))
        }
        hes2 <- -calc_hessian(fn, gr, theta, hndeps, list_args, cl)
        if (any(!is.finite(hes2))) {
            stop(sprintf("Hessian matrix is not finite; iteration %d", i))
        }

        hes2[ind,] <- grad2
        grad2[ind] <- -calc_fn(fn, theta, list_args) - l_star
        theta_diff <- try(solve(hes2, grad2) * damping)
        if (class(theta_diff) == "try-error") {
            stop(sprintf("Error when solving linear simultaneous equation to update parameter; iteration %d", i))
        } else if (any(is.nan(theta_diff))) {
            stop(sprintf("Difference to update parameter is NaN; iteration %d", i))
        }
        theta <- theta - theta_diff
        max_diff <- max(abs(theta_diff))
        if ((trace > 0) && (i %% nreport == 0)) {
            print(sprintf("iteration %d: max_diff=%e", i, max_diff))
        }
        if (max_diff < eps) {
            if (trace > 0) {
                print(sprintf("converged: max_diff=%e", max_diff))
            }
            return(list(convergence = 0, value = theta[[ind]]))
        }
    }
    return(list(convergence = 1, value = theta[[ind]]))
}

##' Function to calculate profile likelihood based confidence interval.
##'
##' @param ind     Index number of par whose confidence interval is calculated
##' @param prob    Probability associated with the (two-sided) confidence interval.
##' @param fn      Function of negative log-likelihood whose first argument is a parameter vector.
##' @param gr      Function of gradient of fn
##' @param ...     Additional arguments of fn and gr
##' @param control Parameter list for calculations of finite difference and Newton method
##' @param cl      Cluster by the package "snow"
##'
##' @return List having two keys "upper" and "lower"
##'
##' @details
##' This function impements the algorithm in \insertCite{VenzonMoolgavkar1988;textual}{optimCL}.
##' The calculations of columns of Hessian matrix are done in parallel if cl is not NULL.
##' Therefore, we can not use processes whose number is more than length(par).
##'
##' control["trace"] is an integer and determines tracing level.
##' The default is 0, which means no output of logs.
##'
##' control["REPORT"] is the frequency of reports. The default is 10.
##'
##' control["maxit"] is maximum number of iterations of Newton method.
##' The default value is 100.
##'
##' control["abstol"] is absolute convergence tolerance of Newton method.
##' The default value is 1e-8.
##'
##' control["damping"] is damping parameter of Newton method,
##' which is a positive coefficient less than one and damps changes in iterations
##' \insertCite{Galantai2000}{optimCL}.
##' Damping parameter is \eqn{\lambda} in iterations of Newton method
##' \deqn{x_{n + 1} = x_{n} - \lambda f(x_n) / f'(x_n).}
##' The default value of damping parameter is 1.0.
##'
##' control["ciside"] is a side of confidence interval and
##' the value is "both", "lower", or "upper".
##' If the value is "lower" or "upper" then only one side of
##' the two-sided confidence interval is calculated.
##' If the value is "both" that is the default value
##' then two sides of the confidence interval are calculated.
##'
##' control["parscale"] is scaling of parameters which affects
##' caluculation of gradient and Hessian matrix, whose default value is rep(1.0, length(par)).
##' control["ndeps"], control["gndeps"], and control["hndeps"] are values of finite difference.
##' control["gndeps"] and control["hndeps"] are for calculations of gradient and
##' Hessian matrix, respectively. If these values are NULL then control["ndeps"] is used.
##' The default values of control["ndeps"], control["gndeps"], and control["hndeps"] are
##' rep(1.0e-3, length(par)), NULL, and NULL, respectively.
##'
##' @references
##' \insertAllCited{}
##'
##' @export
prfLikelihoodCI <- function(par, ind, prob = 0.95, fn, gr = NULL, ..., control = list(), cl = NULL) {
    npar <- length(par)
    if ((ind < 1) || (ind > npar)) {
        stop(sprintf("Invalid parameter ind=%d", ind))
    }
    if (is.null(control[["trace"]])) {
        trace <- 0
    } else {
        trace <- control[["trace"]]
        control <- control[names(control) != "trace"]
    }
    if (is.null(control[["REPORT"]])) {
        nreport <- 10
    } else {
        nreport <- control[["REPORT"]]
        if ((nreport < 1) || (nreport %% 1 != 0)) {
            stop(sprintf("control[\"REPORT\"] must be positive integer; control[[\"REPORT\"]]=%d", control[["REPORT"]]))
        }
        control <- control[names(control) != "REPORT"]
    }
    if (is.null(control[["maxit"]])) {
        max_iteration <- 100
    } else {
        max_iteration <- control[["maxit"]]
        control <- control[names(control) != "maxit"]
    }
    if (is.null(control[["abstol"]])) {
        eps <- 1.0e-8
    } else {
        eps <- control[["abstol"]]
        control <- control[names(control) != "abstol"]
    }
    if (is.null(control[["damping"]])) {
        damping <- 1.0
    } else {
        damping <- control[["damping"]]
        if ((damping <= 0.0) || damping > 1.0) {
            stop("control[\"damping\"] must be between 0.0 and 1.0")
        }
        control <- control[names(control) != "damping"]
    }
    if (is.null(control[["ciside"]])) {
        ciside <- "both"
    } else {
        ciside <- control[["ciside"]]
        control <- control[names(control) != "ciside"]
    }
    if (is.null(control[["parscale"]])) {
        parscale <- rep(1.0, npar)
    } else {
        parscale <- control[["parscale"]]
        control <- control[names(control) != "parscale"]
    }
    if (is.null(control[["ndeps"]])) {
        gndeps_arg <- rep(1.0e-3, npar)
        hndeps_arg <- rep(1.0e-3, npar)
    } else {
        gndeps_arg <- control[["ndeps"]]
        hndeps_arg <- control[["ndeps"]]
        control <- control[names(control) != "ndeps"]
    }
    if (!is.null(control[["gndeps"]])) {
        gndeps_arg <- control[["gndeps"]]
        control <- control[names(control) != "gndeps"]
    }
    if (!is.null(control[["hndeps"]])) {
        hndeps_arg <- control[["hndeps"]]
        control <- control[names(control) != "hndeps"]
    }
    gndeps <- gndeps_arg * parscale
    if (!(all(abs(gndeps) > .Machine $double.eps))) {
        stop("(control[\"ndeps\"] or control[\"gndeps\"]) * control[\"parscale\"] is too small")
    }
    hndeps <- hndeps_arg * parscale
    if (!(all(abs(hndeps) > .Machine $double.eps))) {
        stop("(control[\"ndeps\"] or control[\"hndeps\"]) * control[\"parscale\"] is too small")
    }

    list_args <- list(...)
    qchi <- qchisq(prob, 1)
    l_star <- -calc_fn(fn, par, list_args) - qchi / 2.0

    grad <- -calc_gradient(fn, gr, par, gndeps, list_args, cl)
    if (any(!is.finite(grad))) {
        stop("Gradient at initial parameter is not finite")
    }
    hes <- -calc_hessian(fn, gr, par, hndeps, list_args, cl)
    if (any(!is.finite(hes))) {
        stop("Hessian matrix at initial parameter is not finite")
    }
    hes_submatrix <- hes[-ind, -ind]
    hes_column <- hes[ind, -ind]
    do_db <- -solve(hes_submatrix, hes_column)
    h <- as.vector(sqrt(-qchi / (hes[ind, ind] - t(hes_column) %*% do_db)) / 2.0)
    if (ind == 1) {
        vec <- h * c(1.0, do_db)
    } else if (ind > length(do_db)) {
        vec <- h * c(do_db, 1.0)
    } else {
        vec <- h * c(do_db[1:(ind - 1)], 1.0, do_db[ind:length(do_db)])
    }
    theta_init_upper <- par + vec
    theta_init_lower <- par - vec

    res <- list()
    if (ciside != "lower") {
        if (trace > 0) {
            print("Start calculation of upper")
        }
        res[["upper"]] <- prfLikelihoodCI_newton_step(max_iteration, eps, damping, l_star, theta_init_upper, ind, fn, gr, list_args, gndeps, hndeps, cl, trace, nreport)
    }
    if (ciside != "upper") {
        if (trace > 0) {
            print("Start calculation of lower")
        }
        res[["lower"]] <- prfLikelihoodCI_newton_step(max_iteration, eps, damping, l_star, theta_init_lower, ind, fn, gr, list_args, gndeps, hndeps, cl, trace, nreport)
    }
    return(res)
}
