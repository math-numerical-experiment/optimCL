# optimCL

optimCL provides functions to optimize, to calculate gradient and Hessian matrix,
and to obtain profile likelihood based confidence intervals,
which can calculate in parallel by using a SNOW cluster.

The tests of this library have not done much yet.
Note that only SNOW cluster whose type is SOCK is tested.
In principle, SNOW clusters of other types should work with functions in optimCL, but not tested.

## Installation

    devtools::install_git("https://gitlab.com/math-numerical-experiment/optimCL.git")

## Functions

- optimCL: optimize function
- prfLikelihoodCI: calculate profile likelihood based CI
- gradientCL: calculate gradient
- hessianCL: calculate Hessian matrix
- clusterExportForOptimCL: prepare SNOW cluster for the above functions

Please see help of optimCL for details.
As for parallel computing used in optimCL,
package snow ( https://cran.r-project.org/web/packages/snow/ ) is used.
The only thing that we should do to use computing in parallel is
to confirm evaluations of objective functions work fine on SNOW cluster.
To do so, please see manuals of package snow.

## Example

This is a basic example which shows you how to solve a common problem:

    library(optimCL)
    library(snow)
    
    f <- function(x) {
        x[1] * x[1] + (x[2] - 1) * (x[2] - 1) + (x[3] + 3) * (x[3] + 3)
    }
    
    num_cluster <- 3
    cl <- makeCluster(num_cluster, type = "SOCK")
    clusterExportForOptimCL(cl)
    
    result <- optimCL(c(100, 100, 100), f, cl = cl)
    print(result)

## License

GPL-3
